//
//  connectionGroups.h
//  mocapViz
//
//  Created by jasch on 14/3/2015.
//
//

#ifndef mocapViz_connectionGroups_h
#define mocapViz_connectionGroups_h

class connectionGroups {
public:
	long ID;
	long numConnections;
	string name;
	vector<ofPoint> line;
};

#endif
