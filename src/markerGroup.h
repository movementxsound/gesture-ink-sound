//
//  markerGroup.h
//  mocapViz
//
//  Created by jasch on 8/2/2015.
//
//

#ifndef mocapViz_markerGroup_h
#define mocapViz_markerGroup_h

#include "markerTrack.h"

class markerGroup {
public:
	long count;
	long position;
	string descriptor;
	vector<markerTrack> track;
	vector<double>  timestamp;
};


#endif
