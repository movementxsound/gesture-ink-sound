//
//  markerTrack.h
//  mocapViz
//
//  Created by jasch on 8/2/2015.
//
//

#ifndef __mocapViz__singleMarker__
#define __mocapViz__singleMarker__

#include <iostream>

#include "singleFrame.h"

class markerTrack {
public:
	long count;
	long position;
	long group;
	string name;
	vector<singleFrame> frame;
	
	ofColor colour;
};


#endif /* defined(__mocapViz__singleMarker__) */
