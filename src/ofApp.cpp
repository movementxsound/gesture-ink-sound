#include "ofApp.h"

#define DRAWSKELETON // only for for take 3

void ofApp::setup()
{
	ofSetFrameRate(100);
	ofSetVerticalSync(false);
	ofSetLogLevel(OF_LOG_ERROR);
//	videoPlayer = new(threadedVideoPlayer);
//	videoPlayer->start(); // the videoPlayer thread
	
	// blue gradient
	baseColor[0].set(0, 0.125, 0.35, 1); //background top
	baseColor[1].set(0, 0.045, 0.125, 1); //background bottom
	
	// dark
	baseColor[2].set(1, 1,1, 0.125); // skeleton
	baseColor[3].set(1, 1, 1, 0.35); // text
	baseColor[4].set(1, 1, 1, 0.35); // grid
	baseColor[5].set(1, 1, 1, 0.35); // balls
	
	// greyscale gradient
	baseColor[0].set(0.125, 0.125, 0.125, 1);
	baseColor[1].set(0, 0, 0, 1);
	
	// black background
	baseColor[0].set(0, 0, 0, 1);
	baseColor[1].set(0, 0, 0, 1);

	// alternate colours
	/* white background
	 top.set(255, 255, 255, 255);
	 bottom.set(255, 255, 255, 255);
	 baseColor[2].set(0, 0, 0, 16); // skeleton
	 baseColor[3].set(0, 0, 0, 91); // text
	*/
	
	for(int i = 0; i < 64; i++){
		trailColor[i].set(1.0, 1.0, 1.0, 0.0);
		size[i] = 1.0;
	}
	size[4] = 10.0; // balls
					// axes, grid, trailsTotal, trails, balls, skeleton
	
	trailColor[32].set(1, 0.0, 0.0, 1.0);
	trailColor[34].set(0.0, 0.0, 1.0, 0.25);
	
//	// set 1 reddish
//	trailColor[0].set(1, 0.75, 0.5, 0.25);
//	trailColor[1].set(1, 0.5, 0., 0.25);
//	trailColor[2].set(1, 0.0, 0.0, 0.25);
//
//	// set 2 blueish
//	trailColor[2].set(0, 0.25, 0.5, 0.25);
//	trailColor[1].set(0.2, 0.5, 0.8, 0.25);
//	trailColor[0].set(0.55, 0.65, 0.7, 0.25);
//
//	// set 3 brownish
//	trailColor[2].set(0.5, 0.25, 0., 0.25);
//	trailColor[1].set(0.8, 0.5, 0.1, 0.25);
//	trailColor[0].set(0.7, 0.6, 0.55, 0.25);
//
//	// set 4 grescale
//	trailColor[2].set(0.25, 0.25, 0.25, 0.25);
//	trailColor[1].set(0.5, 0.5, 0.45, 0.25);
//	trailColor[0].set(0.75, 0.75, 0.75, 0.25);
//
//	// set 4 yellow orange red
//	trailColor[2].set(0.75, 0, 0, 0.25);
//	trailColor[1].set(1, 0.5, 0, 0.25);
//	trailColor[0].set(1, 0.9, 0.1, 0.25);
//
//	// set 4 yellow
//	trailColor[2].set(0.75, 0.375, 0.05, 0.25);
//	trailColor[1].set(0.75, 0.5, 0.075, 0.25);
//	trailColor[0].set(0.75, 0.7, 0.1, 0.25);
//
//	// set 4 purple
//	trailColor[3].set(0.2, 0, 0.75, 0.25);
//	trailColor[2].set(0.6, 0.5, 1., 0.25);
//	trailColor[1].set(1, 0.6, 1, 0.25);
	

	
	loadFiles();
	viewScale = 1000.0f;
	
	tex.allocate(1280, 720, GL_RGBA);
	syphonServer.setName("image");
	
	isPaused = false;
	
	TTF.load("automat.ttf", 6, true, true, false);

	startTime = ofGetElapsedTimeMillis();
	
	drawVideo = true;
	drawSkeleton = true;
	drawTrails[0] = true;
	drawBalls = true;
	drawGrid = true;
	drawAxes = true;
	drawTrailsTotal = true;
	
	trailLength = 1250;
	
	camera.setDistance(2500);

	cout << "camera distance is " << camera.getDistance() << endl;
	ofPoint target;
	target.set(0.0, 2000.0, 0.0);
	camera.setTarget(target);
	camera.setFov(45.0f);
	
	orbit_lon = 0.0;
	orbit_lat = -80.0;
	orbit_stepsize = 0.01;
	orbit_radius = 500;
	lookat.setPosition(0.0, 0.0, 0.0);
	
	// OSC remote setup
	receiveport = 12345;
	receiver.setup(receiveport);
	
	// OSC sender setup
	sendport = 12346;
	sender.setup("127.0.0.1", sendport);
}

void ofApp::update(){
	
	unsigned long long now = ofGetElapsedTimeMillis();
	unsigned long long currentPlayTime;
	
	if(isPaused == 0) {
		currentPlayTime = now - startTime;
		position = currentPlayTime / 10;

		position++;
		if(position >= numFrames) {
			startTime = ofGetElapsedTimeMillis();
			position = 0;
		}
	
		if(orbitActive){
			camera.orbit(orbit_lon, orbit_lat, orbit_radius, lookat);
			orbit_lon += orbit_stepsize;

			if(orbit_lon > 90.0 || orbit_lon < -90.){
				orbit_stepsize = orbit_stepsize * -1.0;
			}
		}else{
			lookat = camera.getTarget();
			orbit_radius = camera.getDistance();
		}
		
	}
	receiveOSC();
	sendOSC();
//	cout << "framerate " << ofGetFrameRate() << " position " << (float)position/100.0f <<endl;
}

void ofApp::draw()
{
	ofEnableBlendMode(OF_BLENDMODE_ALPHA);

	ofBackgroundGradient(baseColor[0], baseColor[1], OF_GRADIENT_LINEAR);
	
	ofSetColor(1.0, 1.0, 1.0, 1.0);

	if(drawVideo) {
//		videoPlayer->draw(1280, 720);
	}
	
	ofSetColor(baseColor[3]);
//	TTF.drawString( "time " + ofToString( (float)position/100.0), ofGetWidth()-64.0, ofGetHeight()-10.0);
	TTF.drawString( "time " + ofToString( (float)position/100.0), ofGetWidth()-64.0, ofGetHeight()-10.0);

	camera.begin();
	
	ofSetLineWidth(size[0]);

	// coordinate cross
	if(drawAxes){
		
		// X-axis
		ofSetColor(191, 0, 0, 127);
		ofDrawLine(-viewScale*0.125, 0., 0., viewScale*0.125, 0., 0.);
		ofDrawLine(viewScale*0.125, 0., 0., viewScale*0.1, viewScale*0.0166665, 0. );
		ofDrawLine(viewScale*0.125, 0., 0., viewScale*0.1, -viewScale*0.0166665, 0. );
		
		// Y-axis
		ofSetColor(0, 191, 0, 127);
		ofDrawLine(0, -viewScale*0.125, 0, 0., viewScale*0.125, 0.);
		ofDrawLine(0, viewScale*0.125, 0., viewScale*0.0166665, viewScale*0.1, 0.);
		ofDrawLine(0, viewScale*0.125, 0., -viewScale*0.0166665, viewScale*0.1, 0);

		// Z-axis
		ofSetColor(0, 0, 191, 127);
		ofDrawLine(0, 0, -viewScale*0.125, 0, 0, viewScale*0.125);
		ofDrawLine(0, 0, viewScale*0.125, viewScale*0.0166665, 0, viewScale*0.1);
		ofDrawLine(0, 0, viewScale*0.125, -viewScale*0.0166665, 0, viewScale*0.1);
	}
	
	ofSetLineWidth(size[1]);
	
	if(drawGrid) {
		ofSetColor( baseColor[4] );
		ofDrawLine(-viewScale, 0, 0,				viewScale, 0, 0);
		ofDrawLine(-viewScale, 0, -viewScale,		viewScale, 0, -viewScale);
		ofDrawLine(-viewScale, 0, -viewScale*0.5,	viewScale, 0, -viewScale*0.5);
		ofDrawLine(-viewScale, 0, viewScale*0.5,	viewScale, 0, viewScale*0.5);
		ofDrawLine(-viewScale, 0, viewScale,		viewScale, 0, viewScale);
		
		ofDrawLine(0, 0, -viewScale,				0, 0, viewScale);
		ofDrawLine(-viewScale, 0, -viewScale,		-viewScale, 0, viewScale);
		ofDrawLine(-viewScale*0.5, 0, -viewScale,	-viewScale*0.5, 0, viewScale);
		ofDrawLine(viewScale*0.5, 0, -viewScale,	viewScale*0.5, 0, viewScale);
		ofDrawLine(viewScale, 0, -viewScale,		viewScale, 0, viewScale);
	}

	ofSetLineWidth(size[2]);

	if(drawTrailsTotal){
		ofPushMatrix();
		ofScale(1.0, 1.0, 1.0); // to make 2D side views to one axis to 0.0
		for(int i = 0; i < numMarkers; i++) {
			ofSetColor( trailColor[i] );
			line[i].draw();
		}
	}
	
	ofSetLineWidth(size[3]);
	
	if(drawTrails[0]) {
		ofPushMatrix();
		ofScale(1.0, 1.0, 1.0); // to make 2D side views to one axis to 0.0

		for(int i = 0; i < numMarkers; i++) {
			
			ofSetColor( trailColor[i] );
			
			long outFrame = position - trailLength;
			if(outFrame < 1) outFrame = 1;
			
			trail[i].clear();
			trail[i].setupIndicesAuto();
			trail[i].setMode(OF_PRIMITIVE_LINE_STRIP);
			unsigned int j, k;
			
			for(j = outFrame, k = 0; j < position; j++, k++){
				if( !( track[i].frame[j].point.x == 0.0 && track[i].frame[j].point.y == 0.0 && track[i].frame[j].point.z == 0.0) ){ // don't add at origo

					trail[i].addVertex( track[i].frame[j].point );
					trail[i].addIndex(k);
					trail[i].addColor( ofFloatColor(trailColor[i+32].r,trailColor[i+32].g,trailColor[i+32].b, (float)k/(float)trailLength*trailColor[i+32].a )  );
				}else{
					k--;
				}
			}
			trail[i].draw();
		}
	}
	
	
	if(drawBalls) {
		ofPushMatrix();
		ofScale(1.0, 1.0, 1.0); // to make 2D side views to one axis to 0.0
		
		for(int i = 0; i < numMarkers; i++) {
			if( !( track[i].frame[position].point.x == 0.0 && track[i].frame[position].point.y == 0.0 && track[i].frame[position].point.z == 0.0) ){ // don't draw at origo
				ofSetColor( baseColor[5]  );
				ofDrawIcoSphere( track[i].frame[position].point, size[4] );
			}
		}
	}
	
	ofSetLineWidth(size[5]);
	
	if(drawSkeleton){
		
#ifndef DRAWSKELETON
		ofPushMatrix();
		ofScale(1.0, 1.0, 1.0); // to make 2D side views to one axis to 0.0
		skeleton.clear();
		skeleton.setupIndicesAuto();
		skeleton.setMode(OF_PRIMITIVE_LINE_STRIP);

		for(int i = 0; i < numMarkers; i++) {
			if( !( track[i].frame[position].point.x == 0.0 && track[i].frame[position].point.y == 0.0 && track[i].frame[position].point.z == 0.0) ){ // don't add at origo
				skeleton.addVertex( track[i].frame[position].point );
				skeleton.addIndex(i);
				skeleton.addColor( baseColor[2] );
			}
		}
		skeleton.draw();

#else
		for(int i = 0; i < group.size();i++) {
			for(int j = 0; j < group[i].line.size(); j++){
				
				ofSetColor( baseColor[2], 127 );
				
				ofLine( track[ (int)group[i].line[j].x ].frame[ position ].point, track[ (int)group[i].line[j].y ].frame[ position ].point );
			}
		}
#endif
	}

	ofSetLineWidth(1.0);

	ofPopMatrix();
	camera.end();

	tex.loadScreenData(0, 0, 1280, 720);
	ofSetColor(255, 255, 255);
	ofEnableAlphaBlending();
	syphonServer.publishTexture(&tex);

}


bool ofApp::clearGroups()
{
}

bool ofApp::clearMarkers()
{
}

bool ofApp::addFrame()
{
}

bool ofApp::loadFiles()
{
//	string filePath = "ellen_take8.csv";
//	string filePath = "Pression_take8_MTB_20150311.tsv";
//	string filePath = "real_take_2_trombone_markers_synced_MTB.tsv";
//	string filePath = "take_1_brushes_MTB.tsv";
//	string filePath = "take_1_brushes_canvas_MTB.tsv";
//	string filePath = "take_2_brushes_canvas_MTB.tsv";
// string filePath = "take_5_brush_MTB.tsv";

	string filePath = "Take_3_cleaned_markers_MTB.tsv";

	
	ofFile file(filePath);
	
	if(!file.exists()){
		ofLogError("The file " + filePath + " is missing");
	}else{
		cout << "mocap file loaded" << endl;
	}
	ofBuffer buffer(file);
	getHeaderInfo(buffer);
	setUpMarkerTracks(numMarkers);
	parseFrames(buffer);
	calcDerivatives();
	createLines();
	
	string filePath2 =  "Take_3_connections.tsv";
//	string filePath2 =  "Pression_take8_connections_20150314.tsv";
//	string filePath2 =  "real_take_2_trombone_connections.tsv";
	ofFile file2(filePath2);
	if(!file2.exists()){
		ofLogError("The file " + filePath2 + " is missing");
	}else{
		cout << "connection file loaded" << endl;
	}
	ofBuffer buffer2(file2);
	parseConnections(buffer2);
	
	
//	string filePath3 =  "marker_colours.tsv";
//	ofFile file3(filePath3);
//	if(!file3.exists()){
//		ofLogError("The file " + filePath3 + " is missing");
//	}else{
//		cout << "colour file loaded" << endl;
//	}
//	ofBuffer buffer3(file3);
//	parseColours(buffer3);
	
//	string moviePath = "Pression_20121205_take_08.mov";
//	if(moviePath.size()>0){
//		videoPlayer->isLoaded = videoPlayer->loadVideo( moviePath );
//		videoPlayer->isLoaded != videoPlayer->isLoaded;
//		if(videoPlayer->isLoaded) {
//			videoPlayer->startPlayback();
//			videoPlayer->stopPlayback();
//			
//			videoPlayer->setFrame(0);
//			videoPlayer->frameTime =  videoPlayer->frameOffsetTime = 0;
//			videoPlayer->updateVideo();
//			
//			videoDuration = videoPlayer->getDuration();
//		}
//	}
}

bool ofApp::getHeaderInfo(ofBuffer buffer)
{
	
	while (!buffer.isLastLine()) {
		string line = buffer.getNextLine();
		// cout << "line: " << line << endl;
		
		//Split line into strings
		vector<string> word = ofSplitString(line, "\t");
		
		if(word[0] == "NO_OF_FRAMES") {
			numFrames = atoi( word[1].c_str() );
		}else if(word[0] == "NO_OF_MARKERS") {
			numMarkers = atoi( word[1].c_str() );
		}else if(word[0] == "FREQUENCY") {
			frequency = atoi( word[1].c_str() );
		}else if(word[0] == "DESCRIPTION") {
			description = word[1];
		}else if(word[0] == "TIME_STAMP") {
			timeStamp = word[1];
		}else if(word[0] == "DATA_INCLUDED") {
			dimensions = word[1];
		}else if(word[0] == "MARKER_NAMES") {
			markerNamesLine = line;
		}else if(word[0] == "Frame") {
			fieldNamesLine = line;
		}
	}
	printf("header:\nnumFrames %ld\nnumMarkers %ld\nfrequency %d\ndescription %s\ntimeStamp %s\ndimensions %s\nmarkerNamesLine %s\nfieldNamesLine %s\n",
			   numFrames,
			   numMarkers,
			   frequency,
			   description.c_str(),
			   timeStamp.c_str(),
			   dimensions .c_str(),
			   markerNamesLine.c_str(),
			   fieldNamesLine.c_str()
			   );

	return true;
}

bool ofApp::setUpMarkerTracks(int numMarkers)
{
	string name;
	vector<string> word = ofSplitString(markerNamesLine, "\t");

	cout << "markerName in track creation: " << endl;

	for( int i = 0; i < numMarkers; i++)
	{
		markerTrack tempTrack;
		ofPolyline tempLine;

		tempTrack.name = name = word[i+1];
		if(name.at(0) == 'B'){
			if(name.at(1) == '1'){	//} || name.at(1) == 'F') {
				tempTrack.group = 0;
			}else if(name.at(1) == '2'){ //} || name.at(1) == 'B') {
				// the cello
				tempTrack.group = 1;
			}
		}else if( name.at(0) == 'C'){
			if(name.at(1) == 'L'){
				tempTrack.group = 2;
			}else if(name.at(1) == 'R'){
				tempTrack.group = 3;
			}
		}else if( name.at(0) == 'L'){
			// the left side
			tempTrack.group = 3;
		}else if( name.at(0) == 'R'){
			// the right side
			tempTrack.group = 4;
		}else if( name.at(0) == 'T'){
			// middle of the back
			tempTrack.group = 5; // middle of the back
		}
		cout << "track " << i << " "<< tempTrack.name << endl;
		
		track.push_back(tempTrack);
		line.push_back(tempLine);
	}
}

#pragma mark -
#pragma mark parsing

bool ofApp::parseFrames(ofBuffer buffer)
{
	int i, j;
	double x, y, z;
	singleFrame tempFrame;
	long frameID, timeStamp;
	long counter = 0;

	int frameMarkerCount;

	string frameLine;
	
	for(auto frameLine: buffer.getLines()){
	
	
//	while (!buffer.isLastLine()) {
//		string frameLine = buffer.getNextLine();
//		
//		
		
		vector<string> frameWord = ofSplitString(frameLine, "\t");
		
		if( isInteger(frameWord[0]) ) // we have left the header and are now in the frames-section
		{
			frameID = strtol(frameWord[0].c_str(), NULL, 10);
			timeStamp = std::atof(frameWord[1].c_str());

			for(int i = 0; i < numMarkers; i++) {
				j = (i * 3) + 2;
				tempFrame.frameID = frameID-1;
				tempFrame.timestamp = timeStamp;
				tempFrame.point.x = std::atof(frameWord[j].c_str()) * 1000.;
				tempFrame.point.y = std::atof(frameWord[j+1].c_str()) * 1000.;
				tempFrame.point.z = std::atof(frameWord[j+2].c_str()) * 1000.;
				track[i].frame.push_back( tempFrame ); // Important, the marker indices in the tsv and the indices are not from the same base: MATLAB vs. C++ array notation
			}
			counter++;
		}
	}
	cout << "parsed " << counter << " frames" << endl;
}

bool ofApp::parseConnections(ofBuffer buffer)
{
	connectionGroups tempGroup;
	ofPoint tempPoint;
	int numConnections = 0;
	int connectionCount = 0;
	int groupCount = 0;


	string line;
	
	for(auto line: buffer.getLines()){
	
	//	while (!buffer.isLastLine()) {
//		string line = buffer.getNextLine();

		vector<string> word = ofSplitString(line, "\t");
				
		if(word[0] == "NO_GROUPS") {
			numGroups = atoi( word[1].c_str() );
			cout << "number of groups in file " << numGroups << endl;
		
		}else if(word[0] == "GROUP") {
			tempGroup.ID = -1;
			tempGroup.numConnections = 0;
			tempGroup.name = "";
			tempGroup.line.clear();
			tempGroup.ID = atoi( word[1].c_str() );
			connectionCount = 0;
			groupCount++;
		}else if(word[0] == "NAME") {
			tempGroup.name = word[1];
		}else if(word[0] == "NO_OF_CONNECTIONS") {
			tempGroup.numConnections = atoi( word[1].c_str() );
			group.push_back(tempGroup);
			connectionCount = 0;
		}else if(word[0] == "PAIR") {
			tempPoint.x = (int)strtod(word[1].c_str(), NULL );
			tempPoint.y = (int)strtod(word[2].c_str(), NULL );
			group[groupCount-1].line.push_back(tempPoint);
			cout << "tempPoint " << tempPoint.x << " " << tempPoint.y << endl;
  			connectionCount++;
			if(groupCount >= numGroups && connectionCount > group[groupCount-1].numConnections){
				break;
			}
		}
	}
	cout << "parsed " << groupCount << " groups" << endl;
	for(int i = 0; i < group.size();i++) {
		cout << "group ID " << group[i].ID << "\nname " << group[i].name << "\nnumConenctions " << group[i].numConnections << endl;
		for(int j = 0; j < group[i].line.size(); j++){
			cout << "pair " << group[i].line[j].x << " " << group[i].line[j].y << endl;
		}
	}
}

bool ofApp::parseColours(ofBuffer buffer)
{
	int i;
	ofColor colour;
	int counter = 0;
	int alpha;
	string line;
	
	
	for(auto line: buffer.getLines()){
//		myLine = line.asString();
//	}
//	
//	while (!buffer.isLastLine()) {
//		string line = buffer.getNextLine();
		
		vector<string> word = ofSplitString(line, "\t");
		
		if(word[0] == "ALPHA"){
			alpha = atoi( word[1].c_str() );
		
		}else{

			i = atoi( word[0].c_str() );
			if(i >= 0){
			track[i].colour.r = atoi( word[2].c_str() );
			track[i].colour.g = atoi( word[3].c_str() );
			track[i].colour.b = atoi( word[4].c_str() );
			track[i].colour.a = alpha;
			counter++;
			}else{
//				break;
			}
		}
	}
	cout << counter << " colours loaded" << endl;
}

#pragma mark -
#pragma mark calculate

bool ofApp::calcDerivatives()
{
	ofVec3f p1, p2, delta1;
	double delta2, delta3;
	int i, j;

	for(i = 0; i < numMarkers; i++) {
		
		p1.x = track[i].frame[0].point.x;
		p1.y = track[i].frame[0].point.y;
		p1.z = track[i].frame[0].point.z;
		
		track[i].frame[0].distance = 0.0;
		track[i].frame[0].velocity = 0.0;
		track[i].frame[0].acceleration = 0.0;
				
		for(j = 1; j < track[i].frame.size(); j++) {
			
			p2.x = track[i].frame[j].point.x;
			p2.y = track[i].frame[j].point.y;
			p2.z = track[i].frame[j].point.z;
			
			delta1 = p1 - p2;
			track[i].frame[j].distance = delta1.length(); // distance : pythagoras
			p1 = p2;
			// velocity
			track[i].frame[j].velocity = fabs( track[i].frame[j].distance - track[i].frame[j-1].distance );
			// acceleration
			track[i].frame[j].acceleration = fabs( track[i].frame[j].velocity - track[i].frame[j-1].velocity );
		}
	}
	cout << "calculated derivatives for  " << j << " frames" << endl;

}

bool ofApp::createLines()
{
	for(int i = 0; i < numMarkers; i++) {
		line[i].clear();
		ofPoint temp;
		temp.x = track[i].frame[0].point.x;
		temp.y = track[i].frame[0].point.y;
		temp.z = track[i].frame[0].point.z;
		
		line[i].addVertex( temp );
		
		for(int j = 1; j < track[i].frame.size(); j++){
			
			ofPoint temp;
			temp.x = track[i].frame[j].point.x;
			temp.y = track[i].frame[j].point.y;
			temp.z = track[i].frame[j].point.z;
			
			line[i].addVertex( temp );
			
		}
		line[i].close();
	}
}

bool ofApp::createTrails(long first, long last)
{
	for(int i = 0; i < numMarkers; i++) {

//		trail[i].clear();
//		ofPoint temp;
//		temp.x = track[i].frame[first].point.x;
//		temp.y = track[i].frame[first].point.y;
//		temp.z = track[i].frame[first].point.z;
//		temp = track[i].frame[first].point;
//		trail[i].addVertex( temp );
//		for(int j = last+1; j < first; j++){
//			ofPoint temp;
//			temp.x = track[i].frame[j].point.x;
//			temp.y = track[i].frame[j].point.y;
//			temp.z = track[i].frame[j].point.z;
//			temp = track[i].frame[j].point;
//			trail[i].addVertex( temp );
//		}
//		trail[i].close();
		
		
		//trail[i].clear();
		for(int j = first; j < last; j++){
			trail[i].addVertex( track[i].frame[j].point );
			
		}
//		trail[i].close();
	}
}


#pragma mark -
#pragma mark interact

void ofApp::keyPressed(int key){
	if(key == 'z'){
		position = 0;
		startTime = ofGetElapsedTimeMillis();

	}else if(key == 'p'){
		isPaused = !isPaused;
		
//		if(isPaused ){
//			isPaused = false;
//			videoPlayer->resumePlayback();
//		}else{
//			isPaused = true;
//			videoPlayer->pausePlayback();
//
//		}
	}else if(key == 'c'){
		drawTrails[0] = !drawTrails[0];
	}else if(key == 's'){
		drawSkeleton = !drawSkeleton;
	}else if(key == 'v'){
		drawVideo = !drawVideo;
	}else if(key == 'b'){
		drawBalls = !drawBalls;
	}else if(key == 'f'){
		drawTrailsTotal = !drawTrailsTotal;
	}else if(key == 'g'){
		drawGrid = !drawGrid;
	}else if(key == 'a'){
		drawAxes = !drawAxes;
	}else if(key == 'o'){
		orbitActive = !orbitActive;
	}else if(key == 'r'){
		startTime = ofGetElapsedTimeMillis();
	}
	
}

void ofApp::keyReleased(int key){

}

void ofApp::mouseMoved(int x, int y){

}

void ofApp::mouseDragged(int x, int y, int button){

}

void ofApp::mousePressed(int x, int y, int button){

}

void ofApp::mouseReleased(int x, int y, int button){

}

void ofApp::windowResized(int w, int h){

}

void ofApp::gotMessage(ofMessage msg){

}

void ofApp::dragEvent(ofDragInfo dragInfo){ 

}

#pragma mark -
#pragma mark utils

inline bool ofApp::isInteger(const std::string & s)
{
	if(s.empty() || ((!isdigit(s[0])) && (s[0] != '-') && (s[0] != '+'))) return false ;
	
	char * p ;
	strtol(s.c_str(), &p, 10) ;
	
	return (*p == 0) ;
}


void ofApp::receiveOSC()
{
	string temp;
	float displayFloat;
	int actionFlag = 0;

	while(receiver.hasWaitingMessages()){
		ofxOscMessage m;
		receiver.getNextMessage(m);
		if(m.getAddress() ==  "/viewer/network/receiver/port") {
			receiveport = m.getArgAsInt32( 0 );
			receiver.setup( receiveport );
			actionFlag = 1;
		}else if(m.getAddress() ==  "/viewer/trailColor") { // -> /viewer/color 1 0.5 0.5 0.5 1.
			double r, g, b, a;
			int i = m.getArgAsInt( 0 );
			r = m.getArgAsFloat( 1 );
			g = m.getArgAsFloat( 2 );
			b = m.getArgAsFloat( 3 );
			if(m.getNumArgs() >= 5) {
				a = m.getArgAsFloat( 4 );
			}
			trailColor[i].set(r, g, b, a);
//			printf("color received %d: %f %f %f %f", i, r, g, b, a);
		}else if(m.getAddress() ==  "/viewer/baseColor") { // -> /viewer/color 1 0.5 0.5 0.5 1.
			double r, g, b, a;
			int i = m.getArgAsInt( 0 );
			r = m.getArgAsFloat( 1 );
			g = m.getArgAsFloat( 2 );
			b = m.getArgAsFloat( 3 );
			if(m.getNumArgs() >= 5) {
				a = m.getArgAsFloat( 4 );
			}
			baseColor[i].set(r, g, b, a);
		}else if(m.getAddress() ==  "/viewer/size") { // -> /viewer/size 1 1.0
			double f;
			int i = m.getArgAsInt( 0 );
			f = m.getArgAsFloat( 1 );
			size[i] = (f);
		} else if(m.getAddress() ==  "/viewer/trailLength") {
			trailLength = m.getArgAsInt( 0 );
		} else if(m.getAddress() ==  "/viewer/setPosition") {
			position = m.getArgAsInt( 0 );
			if(position >= numFrames) {
				position = numFrames;
			}else if (position < 0 ){
				position = 0;
			}
		} else if(m.getAddress() ==  "/viewer/setPositionPercent") {
			positionPercent = m.getArgAsFloat( 0 );
			position = numFrames * positionPercent;
			if(position >= numFrames) {
				position = numFrames;
			}else if (position < 0 ){
				position = 0;
			}
			
		} else if(m.getAddress() ==  "/viewer/camera/target") {
			double x = m.getArgAsFloat( 0 );
			double y = m.getArgAsFloat( 1 );
			double z = m.getArgAsFloat( 2 );
			camTarget.set(x, y, z);
			camera.setTarget(camTarget);
		} else if(m.getAddress() ==  "/viewer/camera/fov") {
			double f = m.getArgAsFloat( 0 );
			camera.setFov(f);
		} else if(m.getAddress() ==  "/viewer/camera/orbit") {
			orbit_lon = m.getArgAsFloat( 0 );
			orbit_lat = m.getArgAsFloat( 1 );
			orbit_stepsize = m.getArgAsFloat( 2 );
			orbit_radius = m.getArgAsFloat( 3 );
			camera.setTarget(lookat);
			camera.orbit(orbit_lon, orbit_lat, orbit_radius, lookat);
		} else if(m.getAddress() ==  "/viewer/camera/lookat") {
			double x = m.getArgAsFloat( 0 );
			double y = m.getArgAsFloat( 1 );
			double z = m.getArgAsFloat( 2 );
			lookat.setPosition(x,y,z);
			camera.orbit(orbit_lon, orbit_lat, orbit_radius, lookat);
			camera.setTarget(lookat);

		}else{
			// unrecognized message: display on the bottom of the screen
			string msg_string;
			msg_string += "addr:";
			msg_string = m.getAddress();
			msg_string += " ";
			for(int i = 0; i < m.getNumArgs(); i++){
				// get the argument type
//				msg_string += m.getArgTypeName(i);
//				msg_string += ":";
				// display the argument - make sure we get the right type
				if(m.getArgType(i) == OFXOSC_TYPE_INT32) {
					msg_string += ofToString(m.getArgAsInt32(i));
					msg_string += " ";
				}
				else if(m.getArgType(i) == OFXOSC_TYPE_FLOAT) {
					msg_string += ofToString(m.getArgAsFloat(i));
					msg_string += " ";
				}
				else if(m.getArgType(i) == OFXOSC_TYPE_STRING) {
					msg_string += m.getArgAsString(i);
					msg_string += " ";
				}
				else{
					msg_string += "unknown";
					msg_string += " ";
				}
			}
			printf("unknown osc message received: %s", msg_string.c_str());
		}
//
//		if(actionFlag == 1){
//			writePrefs();
//			return;
//		}else if(actionFlag == 2){
//			
//		}else if(actionFlag == 3){
//			//update GUI and write GUI-XML
//			gui->forceUpdate(true);
//			gui->saveToXml(OFXGUI_XML);
//			return;
//		}
		
	}
}

void ofApp::sendOSC()
{
	int i;
	ofxOscMessage m;
	if( isPaused == 0){
		for(i = 0; i < numMarkers; i++) {
			m.clear();
			m.setAddress( "/mocapViz" );
			m.addIntArg(position);
			m.addStringArg(track[i].name);
			m.addFloatArg(track[i].frame[ position ].point.x);
			m.addFloatArg(track[i].frame[ position ].point.y);
			m.addFloatArg(track[i].frame[ position ].point.z);
			m.addFloatArg(track[i].frame[ position ].distance);
			m.addFloatArg(track[i].frame[ position ].velocity);
			m.addFloatArg(track[i].frame[ position ].acceleration);
			sender.sendMessage( m );
		}
	}
}
