#pragma once

#include <cstdlib>
#include "ofMain.h"
#include "markerTrack.h"
#include "connectionGroups.h"

#include "ofxSyphon.h"
#include "ofxOsc.h"

class ofApp : public ofBaseApp{

public:
	void setup();
	void update();
	void draw();
	
	void keyPressed(int key);
	void keyReleased(int key);
	void mouseMoved(int x, int y);
	void mouseDragged(int x, int y, int button);
	void mousePressed(int x, int y, int button);
	void mouseReleased(int x, int y, int button);
	void windowResized(int w, int h);
	void dragEvent(ofDragInfo dragInfo);
	void gotMessage(ofMessage msg);
	
	bool clearGroups();
	bool clearMarkers();
	bool addFrame();
	
	bool loadFiles();
	bool getHeaderInfo(ofBuffer buffer);
	bool setUpMarkerTracks(int numMarkers);
	
	bool parseFrames(ofBuffer buffer);
	bool parseConnections(ofBuffer buffer);
	bool parseColours(ofBuffer buffer);
	
	bool calcDerivatives();
	bool createLines();
	bool createTrails(long inPosition, long outPosition);
	
//	threadedVideoPlayer * videoPlayer;
//	double videoDuration;
//	double videoPosition;

	inline bool isInteger(const std::string & s);
	
	void receiveOSC();
	void sendOSC();

	long numFrames;
	long numMarkers;
	int frequency;
	string description;
	string timeStamp;
	string dimensions;
	string markerNamesLine;
	string fieldNamesLine;
	
	long numGroups;
	
	vector<string> markerNames;
	vector<string> fieldNames;
	vector<markerTrack> track;
	vector<double>  timestamp;
	
	vector<connectionGroups> group;
	
	vector<int> groupCelloBow;
	vector<int> groupCelloBody;
	vector<int> groupHead;
	vector<int> groupLeft;
	vector<int> groupRight;
	
	
	long count;
	long position;

	//our camera objects for looking at the scene from multiple perspectives
	ofEasyCam camera;
	bool usecamera;
	
	double viewScale;
	
	vector<ofPolyline> line;
	ofMesh trail[64];
	ofMesh skeleton;
	int stepcounter;
	
//	ofColor topColor, bottomColor, skeletonColour, textColour, gridColor;

	ofFloatColor baseColor[64];
	ofFloatColor trailColor[64];
	double size[64];

	ofTexture tex;
    ofxSyphonServer syphonServer;
	
	bool isPaused;
	ofTrueTypeFont	TTF;
	
	unsigned long long startTime;
	double	positionPercent;
	
	bool drawVideo;
	bool drawSkeleton;
	bool drawTrails[16];
	bool drawBalls;
	bool drawGrid;
	bool drawAxes;
	bool drawTrailsTotal;
	
	long trailLength;
	
	bool orbitActive;
	double orbit_lon;
	double orbit_lat;
	double orbit_radius;
	double orbit_stepsize;
	ofNode lookat;
	ofPoint camTarget;
	
	long 	receiveport;
	ofxOscReceiver receiver;
	
	long 	sendport;
	ofxOscSender sender;


};
