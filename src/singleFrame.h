//
//  singleFrame.h
//  mocapViz
//
//  Created by jasch on 8/2/2015.
//
//

#ifndef mocapViz_singleFrame_h
#define mocapViz_singleFrame_h

class singleFrame {
public:
	ofPoint point;
	int		frameID;
	double  timestamp;
	bool	active;
	
	double  distance;
	double  velocity;
	double  acceleration;

};

#endif
